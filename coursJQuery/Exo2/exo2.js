$(function() {
        // Insérer le code jQuery ici les éléments séléctionnés sont affichés en verts

//Q1 retourne le nombre de liens hypertextes contenus dans la page.
console.log('le nombre de liens hypertextes contenus dans la page '+$('link[href]').length);

//Q2 le nombre de balises <ul> de classe bleu.
console.log('le nombre de balises <ul> de classe bleu '+$('ul[class="bleu"]').length);

//Q3 retourne le nombre de balises <li> qui ont un attribut class de valeur impair.
console.log('le nombre de balises <li> de classe impair '+$('li[class="impair"]').length);

//Q4 retourne le premier <li> du <ul> de classe bleu de la page.
console.log('le premier <li> de <ul> class bleu '+$('ul[class="bleu"] > li:first').css({'color':'blue','font-size':'25px'}));

//Q5 retourne la troisieme balise <li> de <ul> de classe rouge.
console.log('la troisieme balise <li> de <ul> de classe rouge '+$('ul[class="rouge"] > li:nth-child(3)').css({'color':'red','font-size':'25px'}))
      });