$(function() {
        // Insérer le code jQuery ici les éléments 
        //séléctionnés sont affichés en verts

//Éléments <ul> directement descendants d'éléments <li>
$('li > ul').css('background-color','orange');

//Éléments <li> directement précédés d'un élément <li>
$('li + li:first').css('color','green');

//Premier élément <li>
$('li:first').css('color','red');

      });