$(function() {
        // Insérer le code jQuery ici les 
        //éléments séléctionnés sont affichés en verts css()

//Q1 Les balises <ul>
console.log('les balises <ul> '+$('ul').length);

//Q2 La balise <ul> de classe bleu
console.log('les balises <ul> qui sont bleu '+ $('ul[class="bleu"]').length);

//Q3 La balise <ul> contenue dans la balise <div>
console.log('les balises <ul> qui sont dans <div> '+$('div ul').length);

//Q4 La balise <li> contenue dans une balise <ul>, elle-même contenue dans une balise <div>, et dont l'attribut class vaut pair
console.log('les balises <li> qui sont dans <ul> qui est dans <div> (impaire) '+$('div > ul > li[class="impair"]').length);

//Q5 Les balises <li> qui possèdent un attribut class
console.log('les balises <li> qui contient class '+$('li[class]').length);


//Q6 Les balises <li> qui possèdent un attribut class de valeur impair
console.log('Les balises <li> qui possèdent un attribut class de valeur impair '+$('li[class="impair"]').length);


      });